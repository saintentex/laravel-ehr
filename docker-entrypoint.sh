#!/bin/bash

set -e
#[ -z "$DB_PASSWORD" ] && echo "ERROR: Need to set POSTGRES_PASSWORD" && exit 1;
DESC="PHP 7.3 FastCGI Process Manager"
NAME=php-fpm
PHP_INI=/usr/local/etc/php/php.ini
CONFFILE=/usr/local/etc/php-fpm.conf
DAEMON=$NAME
DAEMON_ARGS="--fpm-config $CONFFILE  -c $PHP_INI --pid /usr/local/var/run/php"
#CONF_PIDFILE=$(sed -n 's/^pid[ =]*//p' $CONFFILE)

show_help() {
    echo """
Usage: docker run <imagename> COMMAND

Commands
start       : Start a normal server
help        : Show this message
"""
}

# Run
case "$1" in
    bash)
        /bin/bash "${@:2}"
    ;;
    start)
        echo "Starting nginx..."
        service nginx restart
        echo "Starting PHP-fpm... ARG $DAEMON_ARGS"
        exec ${DAEMON}
    ;;
    *)
        show_help
    ;;
esac

