<?php
/**
 * Created by PhpStorm.
 * User: saintent
 * Date: 14/5/2019 AD
 * Time: 00:19
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
header('Content-Type: application/json; charset: utf-8');
$db = new \mysqli('dev-dropship.cci-distributor.com', 'ccishp_db', 'ST5utGTZcd', 'ccishp_db', '3308');

if ($db->connect_error) {
    echo "ERROR";
    throw new \Exception('Error: ' . $db->error . '<br />Error No: ' . $db->errno);
}


$sql = "SELECT * from oc_order limit 0, 10";
$query = $db->query($sql);
if (!$db->errno) {
    if ($query instanceof \mysqli_result) {
        $data = array();

        while ($row = $query->fetch_assoc()) {
            $data[] = $row;
        }

        $result = new \stdClass();
        $result->num_rows = $query->num_rows;
        $result->row = isset($data[0]) ? $data[0] : array();
        $result->rows = $data;

        $query->close();

        echo json_encode($result);
    }
} else {
    throw new \Exception('Error: ' . $this->connection->error . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
}

?>