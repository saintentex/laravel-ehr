FROM php:7.3.5-fpm-stretch
LABEL maintainer="saintentex@gmail.com"
ENV TZ=Asia/Bangkok
ENV DEBIAN_FRONTEND=noninteractive
ENV LANG en_US.utf8

RUN apt-get update && \
    apt-get install -y \
        locales \
        tzdata \
        nginx \
        procps \
        zlib1g-dev \
        libfreetype6-dev \
        libmcrypt-dev \
        libjpeg-dev \
        libxml2-dev\
        libzip-dev \
        libpng-dev && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 && \
    echo $TZ > /etc/timezone && \
    rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-configure gd \
            --enable-gd-native-ttf \
            --with-freetype-dir=/usr/include/freetype2 \
            --with-png-dir=/usr/include \
            --with-jpeg-dir=/usr/include && \
    docker-php-ext-install gd

RUN docker-php-ext-install pdo pdo_mysql mysqli mbstring zip xml ctype bcmath

COPY settings/nginx/site.conf /etc/nginx/sites-available
COPY settings/nginx/general.conf \
    settings/nginx/letsencrypt.conf \
    settings/nginx/fast_cgi.conf /etc/nginx/nginxconfig/

RUN \
    mkdir /app /deployment && \
    ln -s /etc/nginx/sites-available/site.conf /etc/nginx/sites-enabled && \
    rm /etc/nginx/sites-enabled/default && \
    nginx -t

WORKDIR /deployment
COPY docker-entrypoint.sh /deployment/
RUN chmod a+x /deployment/docker-entrypoint.sh
ADD --chown=www-data:www-data app /app

# Entrypoint
ENTRYPOINT ["/deployment/docker-entrypoint.sh"]

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["start"]