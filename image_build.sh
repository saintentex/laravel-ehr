#!/usr/bin/env bash

echo "Laravel EHR App";
PS3='Please enter action: '
options=("Build" "Push" "Start" "Bash" "RUN BASH" "CONTAINER_PRUNE" "IMAGE_PRUNE" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Build")
            case $1 in
            '-t' | "--tag")
                shift
                tag_name=$1
                echo "Tag : $tag_name";
                docker build -t saintent/laravel_ehr:$tag_name .
                break;
                ;;
                *)
                echo "latest";
                docker build -t saintent/laravel_ehr .
                break;
                ;;
            esac

            break;
            ;;
        "Push")
            echo "Push app saintent/laravel_ehr"
            case $1 in
            '-t' | "--tag")
                shift
                tag_name=$1
                echo "Tag : $tag_name";
                docker push saintent/laravel_ehr:$tag_name
                break;
                ;;
                *)
                echo "latest";
                docker push saintent/laravel_ehr
                break;
                ;;
            esac

            break;
            ;;
        "Quit")
            break
            ;;
        "Start")
            docker-compose up -d
            break
            ;;
        "Bash")
            docker-compose exec ehr bash
            break
            ;;
        "RUN BASH")
            docker-compose run ehr bash
            break
            ;;
        "CONTAINER_PRUNE")
            docker container prune
            break
            ;;
        "IMAGE_PRUNE")
            docker image prune
            break
            ;;
        *) echo "invalid option $REPLY"
            break
            ;;
    esac
done